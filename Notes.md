 
# Array Scale

https://stackoverflow.com/questions/48121916/numpy-resize-rescale-image/48121996

# Hills

https://bitesofcode.wordpress.com/2016/12/23/landscape-generation-using-midpoint-displacement/

https://gamedev.stackexchange.com/questions/10984/what-is-the-simplest-method-to-generate-smooth-terrain-for-a-2d-game

# Automata

https://gamedevelopment.tutsplus.com/tutorials/generate-random-cave-levels-using-cellular-automata--gamedev-9664

http://www.roguebasin.com/index.php?title=Cellular_Automata_Method_for_Generating_Random_Cave-Like_Levels

# Animation

https://pyganim.readthedocs.io/en/latest/basics.html

https://stackoverflow.com/questions/15098900/how-to-set-the-pivot-point-center-of-rotation-for-pygame-transform-rotate

https://stackoverflow.com/questions/49014660/tinting-an-image-in-pygame/49017847#49017847

# Health Bars

https://codepen.io/ahopkins/pen/beNWWp
https://stackoverflow.com/questions/6394304/algorithm-how-do-i-fade-from-red-to-green-via-yellow-using-rgb-values


# Music

http://freemusicarchive.org/member/Alpha_Hydrae/New_Mix_13709

# MultiProcess example

https://github.com/Earlo/Virvatuli/tree/multiprocess

# Faster collision / rendering...

http://arcticpaint.com/projects/rabbyt/docs/rabbyt/

http://arcticpaint.com/projects/rabbyt/

http://lib2d.com/