from pygame import BLEND_RGBA_SUB
import numpy
cimport numpy
import cython
cimport cython
import scipy.ndimage as ndimage

cdef extern from "math.h":
    double sqrt(double m)
#numpy.ndarray[numpy.float64_t, ndim=2]
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.cdivision(True)
cdef light(int x, int y, float strength, light_map, int w, int h, double intensity=4.0):
    #light_map = world

    cdef int x2
    cdef int y2
    cdef int xx
    cdef int yy
    cdef double r
    cdef float inv_rad
    cdef bint bounds
    cdef float level
    cdef int start_strength
    cdef int end_strength

    start_strength = -<int>strength
    end_strength   = <int>strength

    #cdef double [65][49] light_map = lights

    for x2 in range(start_strength, end_strength):
        for y2 in range(start_strength, end_strength):
            r = sqrt((x2 * x2) + (y2 * y2))
            if r <= strength:
                r = (r - intensity)
                if r > 0.0:
                    inv_rad = (r / strength)
                else:
                    inv_rad = 0.0
            else:
                inv_rad = 1.0

            xx = x + x2
            yy = y + y2

            bounds = 1
            if xx < 0 or yy < 0:
                bounds = 0

            if xx > w or yy > h:
                bounds = 0

            if bounds:
                level = light_map[xx][yy]
                if level == 1.0:
                    light_map[xx][yy] = inv_rad
                else:
                    if inv_rad < level:
                        light_map[xx][yy] = inv_rad

    #lights = light_map
    #return light_map

@cython.boundscheck(False)
@cython.wraparound(False)
def lighting(day, filter, screen, int screen_width, int screen_height, int camera_x, int camera_y, int mouse_x, int mouse_y, world):
    #filter.fill(day_mask)
    #filter.fill((0,0,0))

    cdef bint block_tiles
    cdef bint day_mask
    cdef int x_mod
    cdef int y_mod
    cdef int x
    cdef int y
    cdef int tiles_x
    cdef int tiles_y
    cdef int xx
    cdef int yy
    cdef int p_x
    cdef int p_y
    cdef int screen_x
    cdef int screen_y
    cdef int v
    cdef int width
    cdef int height
    cdef list scene_lights

    #blit_list = []

    #blit_list.append((light, (mouse_pos[0] -150, mouse_pos[1] -150)))

    #screen_x = player.x - camera.x
    #screen_y = player.y - camera.y
    #pos = (screen_x - 150, screen_y -150)
    #blit_list.append((light, pos))

    width = screen_width
    height = screen_height

    block_tiles = 1
    if block_tiles:
        x_mod = camera_x % 16
        y_mod = camera_y % 16
        y = camera_y // 16
        x = camera_x // 16

        tiles_y = ((height // 16) + y) + 2
        tiles_x = ((width // 16) + x) + 2

        xx = (mouse_x // 16)
        yy = (mouse_y // 16)

        day_mask = 0
        if day_mask:

            def test_func(values):
                return values.mean()

            footprint = numpy.array(
                [[1,0,0],
                [1,0,1],
                [1,0,0]]
            )

            lights = numpy.clip(numpy.array(world.tiles[x:tiles_x, y:tiles_y], copy=True).astype(float), a_min = 0.0, a_max = 1.0)
            scene_lights = ndimage.generic_filter(lights.astype(float), test_func, footprint=footprint).tolist()
            #scene_lights = numpy.average(lights.astype(float))
        else:
            scene_lights = world.lights[x:tiles_x, y:tiles_y].tolist()
            light(xx, yy, 40.0, scene_lights, (width // 16), (height // 16))
            #light(20, 20, 20.0, scene_lights, (width // 16)-1, (height // 16)-1)
            #light(20, 25, 20.0, scene_lights, (width // 16)-1, (height // 16)-1)
            #light(20, 30, 20.0, scene_lights, (width // 16)-1, (height // 16)-1)
            #light(20, 40, 20.0, scene_lights, (width // 16)-1, (height // 16)-1)

        filter.lock()
        day_time = day * 196
        filter.fill((day_time,day_time,day_time))
        for xx in range(x, tiles_x):
            for yy in range(y, tiles_y):
                p_x = xx - x
                p_y = yy - y
                ll = scene_lights[p_x][p_y]
                if ll < 1.0:
                    screen_x = ((xx - x) * 16) - x_mod
                    screen_y = ((yy - y) * 16) - y_mod
                    rect_pos = (screen_x, screen_y, 16, 16)
                    v = ll * day_time
                    filter.fill((v,v,v), rect_pos)
        filter.unlock()

    screen.blit(filter, (0, 0), special_flags=BLEND_RGBA_SUB)#