import pygame
from pygame.math import Vector2 as vec
from objects.sprite import Sprite
from objects.items.dirt import Dirt
from objects.items.bullet import Bullet

class Player(Sprite):
    """
    Main player class, all player related actions are in here
    """
    def __init__(self, animation, rect, world, sound):
        super().__init__(animation, rect, world)
        self.offset_y     = 3
        self.run_speed    = 12
        self.walk         = 6

        self.jumping      = 1
        self.facing       = 0
        self.items        = [Dirt(world, stack=999), Dirt(world, stack=128), Bullet(world, stack=999)]
        self.equip        = 0

        self.health       = 10
        self.max_health   = 20
        self.health_regen = 0.1
        self.armour       = 0
        self.mana         = 5
        self.max_mana     = 10
        self.mana_regen   = 0.2

        self.buffs        = []

        self.grab_range   = 80
        self.grab_speed   = 5

        self.head         = None
        self.body         = None
        self.legs         = None
        self.arm_left     = None
        self.arm_right    = None
        self.neck         = None

        self.last_hit     = 0
        self.can_hit      = True

        self.hit_sound    = sound

    def mouse_event(self, clicked, x, y):
        if clicked:
            self.use_item(x, y)

    def key_event(self, pressed):
        if pressed[pygame.K_SPACE]:
            self.move_jump()

        if pressed[pygame.K_LEFT]:
            self.move_left()

        if pressed[pygame.K_RIGHT]:
            self.move_right()

        if pressed[pygame.K_UP]:
            self.move_jump()

        if pressed[pygame.K_1]:
            self.equip = 0
        if pressed[pygame.K_2]:
            self.equip = 1
        if pressed[pygame.K_3]:
            self.equip = 2
        if pressed[pygame.K_4]:
            self.equip = 3
        if pressed[pygame.K_5]:
            self.equip = 4
        if pressed[pygame.K_6]:
            self.equip = 5
        if pressed[pygame.K_7]:
            self.equip = 6
        if pressed[pygame.K_8]:
            self.equip = 7
        if pressed[pygame.K_9]:
            self.equip = 8
        if pressed[pygame.K_0]:
            self.equip = 9
    
    def hit(self, damage):
        if self.can_hit:
            self.can_hit  = False
            self.last_hit = self.get_ticks() + 2000
            self.health = self.health - damage
            self.hit_sound.play()

    def regen_health(self):
        pass

    def regen_mana(self):
        pass

    def move_jump(self):
        if self.jumping == 0:
            self.velocity -= vec(0, 28)
            self.jumping = 1

    def move_left(self):
        if self.facing == 1:
            self.facing = 0
            self.animation.flip(True, False)
        self.velocity -= vec(0.5, 0)
        self.max_speed(self.velocity)
        self.moving = 1
        self.animation.play()

    def move_right(self):
        if self.facing == 0:
            self.facing = 1
            self.animation.flip(True, False)
        self.velocity += vec(0.5, 0)
        self.max_speed(self.velocity)
        self.moving = 1
        self.animation.play()

    def max_speed(self, vector):
        # max speed
        if vector.x > self.run_speed:
            vector.x = self.run_speed

        if vector.x < -self.run_speed:
            vector.x = -self.run_speed

    def use_item(self, x, y, item=None):
        if not item:
            if self.equip < len(self.items):
                item = self.items[self.equip]
        if item:
            item.use(self, x, y)
        else:
            self.world.destroy_tile(x, y)
            #pass
        self.items[:] = [item for item in self.items if not item.remove_inventory]

    def add_item(self, new_item):
        if new_item.maxStack > 1:
            found_stack = False
            for item in self.items:
                if not found_stack:
                    if new_item.item_index == item.item_index:
                        if item.stack < item.maxStack:
                            new_stack = item.stack + new_item.stack
                            if new_stack > item.maxStack:
                                item.stack = item.maxStack
                                new_item.stack = new_stack - item.maxStack
                                self.add_item(new_item)
                                found_stack = True
                            else:
                                item.stack = new_stack
                                found_stack = True
            if not found_stack:
                self.items.append(new_item)
        else:
            self.items.append(new_item)


    def pickup_item(self, item):
        self.add_item(item)
        item.remove_world = True

    def update(self):
        super().update()
        if not self.can_hit:
            self.animation.set_alpha(128)
            now = self.get_ticks()
            if now > self.last_hit:
                self.can_hit = True
        else:
            self.animation.set_alpha(255)

        self.collision_tile(self.position, self.velocity)
