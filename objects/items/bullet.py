import pygame
from objects.items import Item
from objects.projectiles.bullet import Bullet as PBullet

class Bullet(Item):

    def __init__(self, world, *args, **kwargs):
        sprite = pygame.image.load("./assets/images/bullet.png").convert()
        super().__init__(sprite, world, *args, **kwargs)

        self.offset_y         = -8

        self.item_index       = 10
        self.name             = "Bullet"
        self.maxStack         = 999
        self.projectile       = PBullet
        self.projectile_speed = 12
        self.cooldown         = 100
        self.damage           = 0
        self.explosive_damage = 1
