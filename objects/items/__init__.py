import pygame
import pyganim
import random
from pygame.math import Vector2 as vec
from objects.sprite import Sprite

class Item(Sprite):

    def __init__(self, sprite, world, world_index=None, *args, **kwagrs):
        frames    = [sprite]
        animation = pyganim.PygAnimation(list(zip(frames, [150] * len(frames))))
        animation.scale((16,16))
        animation.play()
        rect      = pygame.Rect(0, 0, sprite.get_width(), sprite.get_height())
        super().__init__(animation, rect, world)

        self.item_index       = 0
        self.world_index      = world_index
        self.name             = None
        self.maxStack         = kwagrs.get('maxStack', 1)
        self.stack            = kwagrs.get('stack', 1)
        self.createTile       = 0
        self.apply_gravity    = True

        self.sprite           = sprite
        self.sprite_icon      = None
        self.icon_width       = 0
        self.icon_height      = 0
        self.remove_world     = False
        self.remove_inventory = False
        self.autouse          = False
        self.cooldown         = 30
        self.last_cooldown    = 0
        # Item use types
        self.projectile       = False
        self.projectile_speed = 0
        self.use_chance       = 1.0

        self.axe_power        = 0
        self.pickaxe_power    = 0
        self.hammer_power     = 0
        
        self.damage           = 0
        self.explosive_damage = 0

        self.value            = 0
        self.rarity           = 1

        self.health           = 0
        self.health_regen     = 0.0
        self.mana             = 0
        self.mana_regen       = 0.0

        self.duration         = 0
        self.buffs            = []

    def decrease(self, count=1):
        self.stack = self.stack - count
        if self.stack == 0:
            self.remove_inventory = True

    def use(self, player, x, y):
        now = self.get_ticks()
        if now > self.last_cooldown:
            if self.createTile:
                if self.stack > 0:
                    created = self.world.create_tile(x, y, self.createTile)
                    if created:
                        self.decrease()
            
            elif self.projectile:
                chance = random.random()
                if chance < self.use_chance:
                    angle = player.get_angle( vec((x*16) + self.world.offset_x, (y*16) + self.world.offset_y) )
                    projectile = self.projectile(self.world,  angle, self.projectile_speed)
                    projectile.center = player.center
                    projectile.damage += self.damage
                    projectile.explosive_damage += self.explosive_damage
                    self.world.projectiles.append(projectile)
                    self.decrease()
            self.last_cooldown = now + self.cooldown

    def icon(self):
        if not self.sprite_icon:
            if self.sprite.get_width() > 32 or self.sprite.get_height() > 32:
                self.sprite_icon = pygame.transform.scale(self.sprite, (32, 32))
            elif self.sprite.get_width() < 16 or self.sprite.get_height() < 16:
                self.sprite_icon = pygame.transform.scale(self.sprite, (16, 16))
            else:
                self.sprite_icon = self.sprite
        return self.sprite_icon

    def update(self, player):
        super().update()
        player_dist = self.get_distance(player)
        # If there isn't a player within 600px suspend events
        if player_dist < 600:
            # Have we hit the player, and need to be picked up?
            if self.collision_other(player):
                player.pickup_item(self)
            # If gravity applies check and see if we hit any tiles.
            if self.apply_gravity:
                self.collision_tile(self.position, self.velocity)
            # if the player is within range, suck the items towards the player's center
            if player_dist < player.grab_range:
                self.position += self.move_to_point(player.center, player.grab_speed)


    def draw(self, screen, camera):
        super().draw(screen, camera)