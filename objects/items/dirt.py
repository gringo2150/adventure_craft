import pygame
from objects.items import Item

class Dirt(Item):

    def __init__(self, world, *args, **kwargs):
        sprite = pygame.image.load("./assets/images/tiles/dirt_tile.png").convert()
        super().__init__(sprite, world, *args, **kwargs)

        self.item_index = 2
        self.name       = "Dirt Block"
        self.maxStack   = 999
        self.createTile = 2
