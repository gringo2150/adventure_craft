import pygame
import math
from pygame import time
from math import pi, degrees
from pygame.math import Vector2 as vec
from collision import TileCollision

COLLISION_LEFT   = 0
COLLISION_TOP    = 1
COLLISION_RIGHT  = 2
COLLISION_BOTTOM = 3

class Sprite(pygame.sprite.Sprite):

    """
    Base object to managing all the game sprites, every sprite should subclass this

    Ideally this should be incharge of loading it's own assets as well...
    """

    def __init__(self, animation, rect, world):
        # TODO: have animation loading options, so a dict of the vairous sets

        # A name to use in game for this sprite
        self.name          = None

        self.offset_x      = 0
        self.offset_y      = 0

        self._position     = vec(0, 0)
        self.velocity      = vec(0, 0)
        # Do we really need to worry about drawing order?
        self.z             = 0

        self.moving        = 0
        self.direction     = 0

        self.gravity       = 1.6
        self.apply_gravity = True
        self.friction      = 0.9

        self.animation     = animation
        self.rect          = rect
        self.hitbox        = rect
        self.width         = rect.width
        self.height        = rect.height
        self.x             = rect.x
        self.y             = rect.y
        self.world         = world

    @property
    def x(self): 
        return self._position.x
      
    @x.setter
    def x(self, x):
        self.rect.x = x
        self._position.x = x

    @property
    def y(self):
        return self._position.y
      
    @y.setter
    def y(self, y): 
        self.rect.y = y
        self._position.y = y

    @property
    def left(self): 
        return self._position.x
      
    @left.setter
    def left(self, x): 
        self.rect.x = x
        self._position.x = x

    @property
    def top(self): 
        return self._position.y
      
    @top.setter
    def top(self, y): 
        self.rect.y = y
        self._position.y = y
    
    @property
    def right(self): 
        return self._position.x + self.width
      
    @right.setter
    def right(self, right): 
        self._position.x = (right - self.width)

    @property
    def bottom(self): 
        return self._position.y + self.height
      
    @bottom.setter
    def bottom(self, bottom): 
        self._position.y = (bottom - self.height)

    @property
    def center(self):
        x = (self._position.x + (self.width / 2))
        y = (self._position.y + (self.height / 2))
        return vec(x, y)
      
    @center.setter
    def center(self, center):
        x = (center.x - (self.width / 2))
        self._position.x = x
        self.rect.x = x
        y = (center.y - (self.height / 2))
        self._position.y = y
        self.rect.y = y

    @property
    def position(self):
        return self._position
      
    @position.setter
    def position(self, pos):
        self._position = pos
        self.rect.x    = pos.x
        self.rect.y    = pos.y

    def mouse_event(self):
        pass

    def key_event(self, pressed):
        pass

    def move_left(self):
        pass

    def move_right(self):
        pass

    def move_to_point(self, position, speed):
        vector = vec(self.x, self.y)
        vector.rotate_ip(self.get_angle(position) - 180)
        vector.scale_to_length(speed)
        return vector

    def move_to_direction(self, angle, speed):
        vector = vec(self.center).rotate(angle - 180)
        vector.scale_to_length(speed)
        return vector

    def knock_back(self, other, direction, power=10):
        pass

    def collide_world_bounds(self):
        # World bounds
        if self.top < 0:
            return True
        elif self.bottom > self.world.w_height:
            return True
        if self.right > self.world.w_width:
            return True
        elif self.left < 0:
            return True
        return False

    def collision_other(self, other):
        return self.rect.colliderect(other.rect)

    def collision_tile(self, position, velocity):
        new_pos, up, down = TileCollision(self.world, self, self.velocity)
        self.position = new_pos
        if down:
            self.jumping = 0

    def set_world_bounds(self):
        # World bounds
        if self.right > self.world.w_width:
            self.right = self.world.w_width

        if self.bottom > self.world.w_height:
            self.bottom = self.world.w_height

        if self.top < 0:
            self.top = 0

        if self.left < 0:
            self.left = 0

    def get_direction(self, angle):
        return round( 8 * angle / (2*math.pi) + 8 ) % 8

    def get_distance(self, position):
        return math.sqrt((self.center.x - position.x)**2 + (self.center.y - position.y)**2)

    def get_angle(self, position):
        """
        Get the angle towards another position if we were going to move towards it.

        90  up
        180 right
        270 bottom
        0   left
        """
        radians = math.atan2(position.y-self.center.y, position.x-self.center.x)
        angle = degrees(radians) + 180
        return angle

    def get_direction_left(self, position):
        """
        Used to decide if the other position is on our left or right
        """
        if self.center.x > position.x:
            return True
        return False

    def get_direction_right(self, position):
        """
        Used to decide if the other position is on our right or left
        """
        if self.center.x < position.x:
            return True
        return False

    def get_ticks(self):
        """
        Short cut for getting the current game ticks used for event times
        """
        return time.get_ticks()

    def update(self):
        # Update the current direction we are heading towards
        a = math.atan2(self.velocity.x, self.velocity.y)
        self.direction = self.get_direction(a)

        # Check and apply gravity
        if self.apply_gravity:
            gravity = vec(0, self.gravity)
            self.velocity += gravity

        # friction
        if self.velocity.length() != 0:
            self.velocity.scale_to_length(self.velocity.length() * self.friction)

        self.velocity.x = (int(self.velocity.x * 10) / 10)
        self.velocity.y = (int(self.velocity.y * 10) / 10)

    def draw(self, screen, camera):
        screen_x = self.x - camera.x
        screen_y = self.y - camera.y
        self.draw_pos = pos = (screen_x + self.offset_x, screen_y + self.offset_y)
        # Don't want to waste drawing time if we are not in bounds.
        # TODO: Seem to have a bug with this in items?
        if camera.contains(self.rect):
            self.animation.blit(screen, pos)
