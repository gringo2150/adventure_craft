import random
from math import pi, degrees
from pygame import time
from pygame.math import Vector2 as vec

from objects.sprite import Sprite
from objects.healthbar import HealthBar

class Mob(Sprite):

    """
    Base object to managing all the game mobs, basically tracks base attributes
    """

    def __init__(self, animation, rect, world):
        super().__init__(animation, rect, world)
        self.last_ticks   = time.get_ticks()
        self.max_health   = 0
        self.health       = 0
        self.health_regen = 0
        self.armour       = 0
        self.biome        = 0
        self.spawn_chance = 600
        self.remove       = False
        self.drops        = []

        self.healthbar    = HealthBar(self)

    def drop(self):
        if len(self.drops):
            drop, chance = random.choice(self.drops)
            drop.center = self.center
            drop.velocity += vec(12, random.choice([4, -4]))
            self.world.items.append(drop)

    def hit(self, damage):
        if not damage:
            damage = 1
        new_health = self.health - damage
        if new_health <= 0:
            self.remove = True
            self.drop()
        else:
            self.health = new_health

    def update(self, player, projectiles):
        player_dist = self.get_distance(player)
        # Check and see how far we are from the player, if too far kill this mob and don't waste cycles
        if player_dist > 2000:
            self.remove = True
        else:
            super().update()

    def draw(self, screen, camera):
        super().draw(screen, camera)
        # Only draw the bar if they have taken dammage
        if self.health < self.max_health:
            self.healthbar.draw(screen)

