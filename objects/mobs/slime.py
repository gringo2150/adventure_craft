import random
from math import pi, degrees
from pygame.math import Vector2 as vec

from objects.mobs import Mob

from objects.items.dirt import Dirt
from objects.items.bullet import Bullet

class Slime(Mob):

    """
    A slime monster class!
    """

    def __init__(self, animation, rect, world):
        super().__init__(animation, rect, world)
        self.offset_y     = 6
        self.run_speed    = 20
        self.jumping      = 1
        self.last_ticks   = self.get_ticks()
        self.max_health   = 10
        self.health       = 10
        self.drops        = [
            (Dirt(self.world, stack=2), 1.0), 
            (Bullet(self.world, stack=100), 1.0)
        ]

    def move_jump(self):
        if self.jumping == 0:
            self.velocity -= vec(0, 28)
            self.jumping = 1

    def move_left(self):
        self.velocity -= vec(12 + (8 * random.random()), 0)
        self.max_speed(self.velocity)
        self.moving = 1
        self.animation.play()

    def move_right(self):
        self.velocity += vec(12 + (8 * random.random()), 0)
        self.max_speed(self.velocity)
        self.moving = 1
        self.animation.play()

    def max_speed(self, vector):
        # max speed
        if vector.x > self.run_speed:
            vector.x = self.run_speed

        if vector.x < -self.run_speed:
            vector.x = -self.run_speed

    def update(self, player, projectiles):
        super().update(player, projectiles)
        now = self.get_ticks()
        if now > self.last_ticks:
            if self.get_direction_left(player.center):
                self.move_left()
            else:
                self.move_right()
            self.move_jump()
            self.last_ticks = (now + 1500 + (1800 * random.random()))
        if len(projectiles) > 0:
            for proj in projectiles:
                if self.collision_other(proj):
                    self.hit(proj.damage)
                    proj.remove = True

        if self.collision_other(player):
            player.hit(1)
        self.collision_tile(self.position, self.velocity)
