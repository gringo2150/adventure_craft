import pygame
import pyganim
from objects.sprite import Sprite
from collision import TileCollisionBool

class Bullet(Sprite):

    def __init__(self, world, angle, velocity):
        sprite     = pygame.image.load("./assets/images/bullet.png").convert()
        frames     = [sprite]
        animation  = pyganim.PygAnimation(list(zip(frames, [1000] * len(frames))))
        animation.scale((16,16))
        animation.play()
        rect       = pygame.Rect(0, 0, sprite.get_width(), sprite.get_height())
        super().__init__(animation, rect, world)
        self.angle            = angle
        self.speed            = velocity
        self.animation.rotate(-self.angle)
        self.remove           = False
        self.damage           = 1
        self.explosive_damage = 0

    def update(self):
        vector = self.move_to_direction(self.angle, self.speed)
        self.center += vector
        if self.collide_world_bounds():
            self.remove = True
        tile = TileCollisionBool(self.world, self, vector)
        if tile:
            self.remove = True
            if self.explosive_damage > 0:
                self.world.destroy_tile(int(tile[0] // 16), int(tile[1] // 16))

    def draw(self, *args, **kwagrs):
        super().draw(*args, **kwagrs)