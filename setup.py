from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy
from Cython.Distutils import build_ext

ext_modules = [
    #Extension('main', ['main.py']),
    Extension('lights', ['lights.pyx']),
    Extension('collision', ['collision.pyx']),
    #Extension('sprite', ['./objects/sprite.py']),
    #Extension('player', ['./objects/player.py']),
]

setup(
    name='Craft',
    ext_modules=ext_modules,
    cmdclass={'build_ext': build_ext},
    include_dirs=[numpy.get_include()]
)