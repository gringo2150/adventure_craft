import pygame

class Camera(pygame.Rect):

    """
    The view port into the world, helps decide what to draw form the world and active sprite list.
    """

    speed      = 0
    tracking   = None
    bounds     = (500, 300)
    inner_rect = None

    def __init__(self, x, y, w, h, world=None, tracking=None, bounds=None):
        super().__init__(x, y, w, h)
        self.world = world
        if tracking:
            self.tracking = tracking
            self.center = self.tracking.center
        if bounds:
            self.bounds = bounds
        bx = self.x + self.bounds[0]
        by = self.y + self.bounds[1]
        bw = self.width - self.bounds[1]
        bh = self.height - self.bounds[0]
        self.inner_rect = pygame.Rect(bx, by, bw, bh)

        # Update our position
        self.update()

    def update(self):
        self.inner_rect.center = self.center
        if not self.tracking:
            return

        if not self.inner_rect.contains(self.tracking.rect):

            # Top bounds
            if self.tracking.top < self.inner_rect.top:
                new_y = (self.inner_rect.top - self.tracking.top)
                self.top -= new_y
            # Bottom bounds
            if self.tracking.bottom > self.inner_rect.bottom:
                new_y = (self.tracking.bottom - self.inner_rect.bottom)
                self.bottom += new_y

            # Right bounds
            if self.tracking.left < self.inner_rect.left:
                new_x = (self.inner_rect.left - self.tracking.left)
                self.left -= new_x

            # Left bounds
            if self.tracking.right > self.inner_rect.right:
                new_x = (self.tracking.right - self.inner_rect.right)
                self.right += new_x

        # Check world bounds
        if self.top < 0:
            self.top = 0

        if self.bottom > self.world.w_height:
            self.bottom = self.world.w_height

        if self.left < 0:
            self.left = 0

        if self.right > self.world.w_width:
            self.right = self.world.w_width

