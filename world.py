import pygame
import json
import random
import math
import os
import numpy as np

from camera import Camera
from objects.items import Item

# This is our world size
SMALL  = [1024, 4096] # This is 1024 tiles by 4096 tiles.
MEDIUM = SMALL  * 2
LARGE  = MEDIUM * 2

# Layers
SPACE       = 0.1
SURFACE     = 0.2
UNDERGROUND = 0.4
CAVERNS     = 0.2
UNDERWORLD  = 0.1

"""
Layer Name  Small World Medium World    Large World
Space   560 to 464  870 to 610  1356 to 683
Surface 463 to 1    609 to 1    682 to 1
Level Surface   0 to 0  0 to 0  0 to 0
Underground -1 to -194  -1 to -422  -1 to -517
Caverns -195 to -1350   -423 to -2242   -518 to -3276
Underworld  -1351 to -1655  -2243 to -2562  -3277 to -3588
"""

# https://github.com/ProjectET/Terraria-Tile-Generator

class Tile(object):

    block     = 0
    rotation  = 0
    wall      = 0
    grass     = 0
    biome     = 0
    strength  = 0
    placed    = 0
    item      = 0
    drops     = 0
    explosive = 0

    def destroy(self):
        pass

class Layer(object):

    start  = 0
    end    = 0
    blocks = [0]

    def set_bounds(self, start, end):
        self.start = start
        self.end   = end

        return self

    def set_blocks(self, blocks=[0]):
        self.blocks = blocks

        return self

    def in_layer(self, y):
        if y >= self.start and y <= self.end:
            return True
        return False

    def insert(self, seed=False):
        if not seed:
            return random.choice(self.blocks)

        tile_seed = random.random()
        if tile_seed > 0.1:
            return random.choice(self.blocks)

class World(object):
    
    size        = SMALL
    height      = size[0]
    width       = size[1]
    w_height    = size[0] * 16
    w_width     = size[1] * 16
    max_height  = size[0] * 16
    max_width   = size[1] * 16
    offset_x    = 0
    offset_y    = 0
    tile_size   = 16
    seed        = 'isaacs_game'
    tiles       = []
    lights      = []
    layers      = {
        'space': Layer(),
        'surface': Layer(),
        'underground': Layer(),
        'caverns': Layer(),
        'underworld': Layer()
    }
    blocks      = []
    # Used to keep track of dropped items in the world
    items       = []
    projectiles = []
    # The screen for drawing etc
    screen      = None

    def __init__(self, size=SMALL, seed=None, screen=None, blocks=[]):
        self.size = size
        if seed:
            self.seed = seed
        if screen:
            self.screen = screen
        if blocks:
            self.blocks = blocks

    def update_bounds(self):
        size = self.size
        self.height      = size[0]
        self.width       = size[1]
        self.w_height    = size[0] * 16
        self.w_width     = size[1] * 16
        self.max_height  = size[0] * 16
        self.max_width   = size[1] * 16

    def clear(self):
        self.tiles = []

    def build_layers(self):
        # Calculate the layers
        space       = int(self.height * SPACE)
        surface     = int(self.height * SURFACE) + space
        underground = int(self.height * UNDERGROUND)   + surface
        caverns     = int(self.height * CAVERNS) + underground
        underworld  = int(self.height * UNDERWORLD) + caverns

        self.layers['space'].set_bounds(0, space).set_blocks([0])
        self.layers['surface'].set_bounds(space, surface).set_blocks([2])
        self.layers['underground'].set_bounds(surface, underground).set_blocks([1])
        self.layers['caverns'].set_bounds(underground, caverns).set_blocks([3])
        self.layers['underworld'].set_bounds(caverns, underworld).set_blocks([4, 3])

    def ores():
        pass

    def clouds():
        pass

    def caves():
        pass

    def smooth():
        pass

    def houses():
        pass

    def trees():
        pass

    def grass():
        pass

    def place_items():
        pass

    def initialiseMap(self, grid, live_rate=0.4):
        for x in range(0, len(grid)):
            for y in range(0, len(grid[0])):
                if random.random() < live_rate:
                    grid[x][y] = 1
        return grid

    #Returns the number of cells in a ring around (x,y) that are alive.
    def countAliveNeighbours(self, grid, x, y):
        count = 0
        for i in range(-1, 2):
            for j in range(-1, 2):
                neighbour_x = x + i
                neighbour_y = y + j
                #If we're looking at the middle point
                if i == 0 and j == 0:
                    #Do nothing, we don't want to add ourselves in!
                    pass
                #In case the index we're looking at it off the edge of the map
                elif neighbour_x < 0 or neighbour_y < 0 or neighbour_x >= len(grid) or neighbour_y >= len(grid[0]):
                    count = count + 1
                
                #Otherwise, a normal check of the neighbour
                elif grid[neighbour_x][neighbour_y] == 1:
                    count = count + 1

        return count

    def doSimulationStep(self, oldMap):
        h      = len(oldMap[0])
        w      = len(oldMap)
        newMap = [[0 for y in range(h)] for x in range(w)]

        birthLimit = 4
        deathLimit = 3
        #Loop over each row and column of the map
        for x in range(0, w) :
            for y in range(0, h) :
                nbs = self.countAliveNeighbours(oldMap, x, y)
                #print(nbs)
                #The new value is based on our simulation rules
                #First, if a cell is alive but has too few neighbours, kill it.
                if oldMap[x][y] == 1:
                    if nbs < deathLimit:
                        newMap[x][y] = 0
                    else:
                        newMap[x][y] = 1
                #Otherwise, if the cell is dead now, check if it has the right number of neighbours to be 'born'
                else:
                    if nbs > birthLimit:
                        newMap[x][y] = 1
                    else:
                        newMap[x][y] = 0
        return newMap

    def generateMap(self, size, steps=3, live_rate=0.4):
        h       = size[0]
        w       = size[1]
        #Create a new map
        cellmap = [[0 for y in range(h)] for x in range(w)]
        #Set up the map with random values
        cellmap = self.initialiseMap(cellmap, live_rate)
        #And now run the simulation for a set number of steps
        for i in range(0, steps):
            cellmap = self.doSimulationStep(cellmap)

        return cellmap

    def process_cellmap(self, cellmap, tile=0, offset=(0,0)):
        cellmap = self.doSimulationStep(cellmap)
        #self.cellmap = cellmap
        
        # Apply caves
        for x in range(0, self.size[1]):
            for y in range(0, self.size[0]):
                _tile = 0
                try:
                    _tile = cellmap[x][y]
                except:
                    #print(x, y)
                    pass
                if _tile == 1:
                    x = x + offset[0]
                    y = y + offset[1]
                    self.tiles[x][y] = tile

    
    def perlin_noise(self):
        reduction = 0.5

    def generate(self):
        if not self.load():
            random.seed(self.seed)

            self.w_height = self.size[0] * self.tile_size
            self.w_width  = self.size[1] * self.tile_size

            self.build_layers()

            # Basic layer setup for the world
            for x in range(0, self.width):
                tiles = []
                for y in range(0, self.height):
                    tile = 0
                    for key, layer in self.layers.items():
                        if layer.in_layer(y):
                            tile = layer.insert()
                    tiles.append(tile)
                self.tiles.append(tiles)

            small = self.generateMap(self.size, steps=2, live_rate=0.2)

            self.cellmap = self.generateMap(self.size, steps=1, live_rate=0.3)

            self.surface_caves = self.generateMap([256, 4096], steps=6, live_rate=0.45)

            cellmap = self.generateMap([768, 4098], steps=3, live_rate=0.4)

            self.process_cellmap(self.surface_caves, 1, (0, int(self.layers['surface'].start) + 60))

            self.process_cellmap(small, 1)

            self.process_cellmap(self.cellmap)

            self.process_cellmap(cellmap, offset=(0, 255))

            self.save()   

        return self.tiles

    def load(self):
        if os.path.exists("./world.map"):
            with open("./world.map", 'r') as f:
                world = f.read()
                self.tiles  = json.loads(world)
                f.close()
                self.lights = np.array(self.tiles, copy=True).astype(float)
                self.lights.fill(1.0)
                return True
        return False

    def save(self, name='world.map'):
        world = json.dumps(self.tiles)
        with open("./world.map", 'w') as f:
            f.write(world)
            f.close()
    
    def key_event(self, pressed):
        pass

    def event(self, event):
        pass

    def update(self, player):
        # Update the position of any items
        if len(self.items):
            for item in self.items:
                item.update(player)
            self.items[:] = [item for item in self.items if not item.remove_world]
            # TODO, Item grouping, so like item tracking is reduced and items are stacked.
        # Update the position of any projectiles
        if len(self.projectiles):
            for proj in self.projectiles:
                proj.update()
            self.projectiles[:] = [proj for proj in self.projectiles if not proj.remove]

    def check_tile(self):
        pass

    def collision(self, rect):
        pass

    def add_item(self, item):
        # Look to change how we store items to be a dict of lists per region.
        # Need to be smart about the loading of these regions and may sometimes need to load 3 depending on the bounds
        # These should be quite large maybe chunks of double screen sizes, this would make updating much faster.

        # A smarter method may be to have some smart shuffeling, so the items list is like active memory
        # Once this gets to a given size or the player has moved too far we move the item into the region storage
        # This in theory should keep things pretty fast?

        # Another thing to be considered is item stacking, when we have so many of x in one place reduce them to 1 item
        pass

    def remove_item(self, item):
        # Is this needed, generally we clean up from item.remove based flags
        pass

    def destroy_tile(self, x, y):
        # Is it worth adding guarding in for tile coords and take a raw x, y
        tile = self.tiles[x][y]
        if tile:
            self.tiles[x][y] = 0
            item               = Item(self.blocks[tile], self, len(self.items))
            item.item_index    = tile                
            item.maxStack      = 999
            item.stack         = 1
            item.createTile    = tile
            item.x             = (x*16)
            item.y             = (y*16)
            item.apply_gravity = True
            self.items.append(item)

    def create_tile(self, x, y, tile_index):
        tile = self.tiles[x][y]
        if tile == 0:
            self.tiles[x][y] = tile_index
            return True
        return False

    def draw(self, screen, x=0, y=0, blocks=[]):
        # The list of all the tiles we want to draw to the screen
        tile_list = []

        raw_x = x
        raw_y = y

        self.offset_x = x_mod = x % 16
        self.offset_y = y_mod = y % 16
        y = y // 16
        x = x // 16

        screen_width  = screen.get_width()
        screen_height = screen.get_height()

        # We do this so we draw at least one tile off camera
        # We do a check to make sure we don't go out of the array bounds though
        tiles_x = int((screen_width // 16) + x) + 2
        if tiles_x > self.width:
            tiles_x = self.width
        tiles_y = int((screen_height // 16) + y) + 2
        if tiles_y > self.height:
            tiles_y = self.height

        # Can this loop be better optomised?
        for xx in range(x, tiles_x):
            for yy in range(y, tiles_y):
                block = self.tiles[xx][yy]
                # Test for a block, 0 is false and no block so don't waste processing time
                if block:
                    screen_x = ((xx - x) * 16) - x_mod
                    screen_y = ((yy - y) * 16) - y_mod
                    pos = (screen_x, screen_y)
                    tile = blocks[block]
                    if tile:
                        tile_list.append((tile, pos))

        # Blit all at once
        screen.blits(tile_list, doreturn=False)
        #screen.lock()
        # Now do items and projectiles
        cam = Camera(raw_x, raw_y, screen_width, screen_height, self)
        for item in self.items:
            item.draw(screen, cam)

        for proj in self.projectiles:
            proj.draw(screen, cam)
        #screen.unlock()
                    