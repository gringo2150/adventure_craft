import numpy as np

arr = np.random.randint(0, 5, (1024, 4096))

print('full')
print(arr)

sli = arr[0:50,0:40]
print('slice')
print(sli)
index = np.argwhere(sli > 0)
print('index')
print(index)
xy = index * 16
print('x, y')
print(xy)

print('cells')
print(len(xy))

print('values')
values = sli[sli>0]
print(values)
print(len(values))

ll = np.concatenate([xy,values], axis=0)
print(ll)