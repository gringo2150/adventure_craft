from pygame.math import Vector2 as vec2
import cython

@cython.boundscheck(False)
@cython.wraparound(False)
def TileCollision(world, Rect, Velocity, fallThrough = False, fall2 = False):
    up = False
    down = False
    cdef int height = Rect.height
    cdef int width = Rect.width
    velocity = Velocity
    old_pos = vec2(Rect.x, Rect.y)
    new_pos = vec2(Rect.x, Rect.y) + velocity
    
    result = new_pos

    cdef int left    = int((Rect.left // 16) - 1)
    cdef int right   = int((Rect.right // 16) + 2)
    cdef int top     = int((Rect.top // 16) -1)
    cdef int bottom  = int((Rect.bottom // 16) + 2)

    cdef int num5 = -1
    cdef int num6 = -1
    cdef int num7 = -1
    cdef int num8 = -1

    # Bounds checking
    if left < 0:
        left = 0

    if right > world.width:
        right = world.width

    if top < 0:
        top = 0

    if bottom > world.height:
        bottom = world.height

    cdef float num9 = ((float(bottom) + 3.0) * 16.0)
    
    for i in range(left, right):
        for j in range(top, bottom):
            if world.tiles[i][j] > 0:
                tile_pos = vec2(float(i * 16), float(j * 16))
                tile_size = 16.0
                """
                if (world.tiles[i][j].halfBrick())
                
                    tile_pos.y += 8f
                    tile_size -= 8
                
                """
                if (new_pos.x + width) > tile_pos.x and new_pos.x < (tile_pos.x + tile_size) and (new_pos.y + height) > tile_pos.y and new_pos.y < (tile_pos.y + tile_size):
                    if (old_pos.y + height) <= tile_pos.y:
                        #bottom collision
                        down = True
                        if num9 > tile_pos.y:
                            num7 = i
                            num8 = j
                            if tile_size < 16:
                                num8 += 1
                            if num7 != num5:
                                result.y = tile_pos.y - (height + 0.001)
                                num9 = tile_pos.y
                                velocity.y = 0
                    
                    if old_pos.y >= (tile_pos.y + tile_size):
                        #top collision
                        up = True
                        num7 = i
                        num8 = j
                        result.y = tile_pos.y + (tile_size + 0.001)
                        velocity.y = 0
                    
                    if (old_pos.x + width) <= tile_pos.x:
                        #right collision
                        num5 = i
                        num6 = j
                        if num6 != num8:
                            result.x = tile_pos.x - (width + 0.001)
                    
                    if old_pos.x >= (tile_pos.x + 16.0):
                        #left collision
                        num5 = i
                        num6 = j
                        if num6 != num8:
                            result.x = tile_pos.x + 16.001
                            velocity.x = 0
                                
    return (result, up, down)

@cython.boundscheck(False)
@cython.wraparound(False)
def TileCollisionBool(world, Rect, Velocity, fallThrough = False, fall2 = False):
    up = False
    down = False
    cdef int height = Rect.height
    cdef int width = Rect.width
    velocity = Velocity
    old_pos = vec2(Rect.x, Rect.y)
    new_pos = vec2(Rect.x, Rect.y) + velocity

    cdef int left    = int((Rect.left // 16) - 1)
    cdef int right   = int((Rect.right // 16) + 2)
    cdef int top     = int((Rect.top // 16) -1)
    cdef int bottom  = int((Rect.bottom // 16) + 2)

    # Bounds checking
    if left < 0:
        left = 0

    if right > world.width:
        right = world.width

    if top < 0:
        top = 0

    if bottom > world.height:
        bottom = world.height
    
    for i in range(left, right):
        for j in range(top, bottom):
            if world.tiles[i][j] > 0:
                tile_pos = vec2(float(i * 16), float(j * 16))
                tile_size = 16.0
                """
                if (world.tiles[i][j].halfBrick())
                
                    tile_pos.y += 8f
                    tile_size -= 8
                
                """
                if (new_pos.x + width) > tile_pos.x and new_pos.x < (tile_pos.x + tile_size) and (new_pos.y + height) > tile_pos.y and new_pos.y < (tile_pos.y + tile_size):
                    if (old_pos.y + height) <= tile_pos.y:
                        return tile_pos
                    else:
                        if (old_pos.x + width) <= tile_pos.x:
                            return tile_pos
                        else:
                            if old_pos.x >= (tile_pos.x + 16.0):
                                return tile_pos
                            else:
                                if old_pos.y >= tile_pos.y + float(tile_size):
                                    return tile_pos
    return False
