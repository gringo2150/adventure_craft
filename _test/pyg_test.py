import sys
import pyglet
import pygame
import os

from world import World, SMALL
from sprite import Sprite
from camera import Camera

size = width, height = 1024, 768

# Set a world to use
world = World(SMALL)
# Generate the world
world.generate()

screen = pyglet.window.Window(width, height)

# Tiles
dirt    = pyglet.resource.image("./images/tiles/dirt_tile.png")
stone   = pyglet.resource.image("./images/tiles/grassland/terrain_center.png")
grass   = pyglet.resource.image("./images/tiles/grassland/terrain_platform_center.png")
cavern  = pyglet.resource.image("./images/tiles/grassland/midground_center.png")

# Block map
blocks = [None, stone, dirt, cavern]

background      = pyglet.resource.image("./images/backgrounds/menu_back.jpg")
background_size = (background.width, background.height)

animObj = pyglet.image.ImageGrid(pyglet.resource.image('./images/sprites/npc_22.png'), 16, 1)[0]

player = Sprite(animObj, pygame.Rect(0,0,56,40), world, True)
player.x = 60000
player.y = 200

# Main world camera 
cam = Camera(0, 0, width, height, world, player)

# Main game loop
@screen.event
def on_draw():

    #mouse_pos = pygame.mouse.get_pos()

    #snap_x = ((mouse_pos[0] // 16) * 16) - world.offset_x# - (mouse_pos[0] % 16)
    #snap_y = ((mouse_pos[1] // 16) * 16) - world.offset_y# - (mouse_pos[1] % 16)

    #world_x = (((mouse_pos[0] + cam.x) - world.offset_x) // 16)
    #world_y = (((mouse_pos[1] + cam.y) - world.offset_y) // 16)

    #mouse_down = False
    """
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            sys.exit()
        if event.type == pygame.MOUSEBUTTONUP:
            mouse_down = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_down = True
        if event.type == pygame.KEYUP:
            player.animation.pause()

    # This is where we check for key press's
    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_ESCAPE]:
        sys.exit()

    if pressed[pygame.K_LEFT]:
        player.animation = animObj
        player.move_left()

    if pressed[pygame.K_RIGHT]:
        player.animation = animObjr
        player.move_right()

    if pressed[pygame.K_UP]:
        player.move_jump()

    if pressed[pygame.K_DOWN]:
        pass

    player.key_event(pressed)
    """

    player.update(world)

    cam.update()

    # DRAWING CYCLE

    # Draw the main background, replace with a paralax class...
    #screen.blit(background,(0,0))

    # Tile drawing, IE the world
    #world.draw(screen, cam.x, cam.y, blocks)

    # Draw player and other sprites
    screen.clear()
    player.draw(screen, cam)
    
    # Flip the screen and show to the user.
    #pygame.display.flip()
    #clock.tick(99999)
pyglet.app.run()
