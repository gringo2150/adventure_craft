import pyglet

config = pyglet.gl.Config(double_buffer=True)
window = pyglet.window.Window(1024, 768) #, fullscreen=True
image  = pyglet.resource.image('./images/mana.png')

image.width = 16
image.height = 16

@window.event
def on_draw():
    window.clear()
    image.blit(0, (0 + 480) - 16)

pyglet.app.run()