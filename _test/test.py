import pygame
import numpy as np


def gray(im):
    im = 255 * (im / im.max())
    w, h = im.shape
    ret = np.empty((w, h, 3), dtype=np.uint8)
    ret[:, :, 2] = ret[:, :, 1] = ret[:, :, 0] = im
    return ret

pygame.init()
display = pygame.display.set_mode((350, 350))
x = np.arange(0, 300)
y = np.arange(0, 300)
X, Y = np.meshgrid(x, y)
Z = X + Y
Z = 255 * Z / Z.max()
#Z = gray(Z)

xx = np.arange(0, 300)
yy = np.arange(0, 300)
XX, YY = np.meshgrid(xx, yy)
ZZ = XX + YY
ZZ = 255*ZZ/ZZ.max()

ZZZ = ZZ + Z

surf = pygame.surfarray.make_surface(ZZZ)

running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    display.blit(surf, (0, 0))
    pygame.display.update()
pygame.quit()