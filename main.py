import sys, pygame
import random
import math
import numpy
import pyganim
import os

from pygame.math import Vector2 as vec2

from world import World, SMALL
from objects.sprite import Sprite
from objects.player import Player
from objects.mobs.slime import Slime
from camera import Camera
from util import colorize
from lights import lighting

from drawing.rounded import round_rect


size = width, height = 1024, 768

# Colors
white = (255, 255, 255)

pygame.init()
#| pygame.OPENGL
flags = pygame.DOUBLEBUF | pygame.HWSURFACE | pygame.RESIZABLE
screen = pygame.display.set_mode(size)

# Tiles
dirt    = pygame.image.load("./assets/images/tiles/dirt_tile.png").convert()
stone   = pygame.image.load("./assets/images/tiles/grassland/terrain_center.png").convert()
grass   = pygame.image.load("./assets/images/tiles/grassland/terrain_platform_center.png").convert()
cavern  = pygame.image.load("./assets/images/tiles/grassland/midground_center.png").convert()
corrupt = colorize(stone, (128, 40, 128, 128))

# Block map
blocks = [None, stone, dirt, cavern, corrupt]

# Set a world to use
world = World(SMALL, blocks=blocks)
# Generate the world
world.generate()

filter = pygame.surface.Surface(size)

scene = pygame.surface.Surface(size)

background      = pygame.image.load("./assets/images/backgrounds/menu_back.jpg").convert()
background_size = background.get_size()
background_rect = background.get_rect()

# Light fills
light = pygame.image.load('./assets/images/masks/light.png').convert_alpha()
#light.set_colorkey((0,0,0))

# HUD images
heart = pygame.image.load('./assets/images/heart_new.png').convert()
heart.set_colorkey((0,0,0))

heart_none = heart.copy()
heart_none.set_alpha(96)

mana  = pygame.image.load('./assets/images/mana_new.png').convert()
mana.set_colorkey((0,0,0))

mana_none = mana.copy()
mana_none.set_alpha(96)

cursor  = pygame.image.load('./assets/images/cursor.png').convert()
aim     = pygame.image.load('./assets/images/aim.png').convert()

w,h = background_size

x = 0
y = 0

x1 = -(w + 4)
y1 = 0

# Game state
state = 0

pygame.mixer.init()
pygame.mixer.music.load("./assets/sound/music/normal.ogg")
pygame.mixer.music.play(-1)
pygame.event.wait()

hit_sound = pygame.mixer.Sound('./assets/sound/player/hit.wav')

images = pyganim.getImagesFromSpriteSheet('./assets/images/sprites/npc_22.png', rows=16, cols=1, rects=[])

def load_sprite_sheet(path, rows=1, cols=1, flip=False, limit=0):
    sprites = []

    sheet   = pygame.image.load(path).convert_alpha()
    size    = sheet.get_size()
    rect    = sheet.get_rect()

    sprite_width  = int(size[0] / cols)
    sprite_height = int(size[1] / rows)
    for x in range(cols):
        for y in range(rows):
            if limit > 0 and len(sprites) == limit:
                break;

            x      = x * sprite_width
            y      = y * sprite_height
            area   = pygame.Rect((x, y), (sprite_width, sprite_height))
            try:
                sprite = sheet.subsurface(area)
                if flip:
                    sprite = pygame.transform.flip(sprite, True, False)
                sprites.append(sprite)
            except:
                pass
    del sheet
    return sprites

sprl = load_sprite_sheet('./assets/images/sprites/npc_22.png', 16, 1)

frames = list(zip(sprl[2:7], [100] * len(sprl)))
animObj = pyganim.PygAnimation(frames)
animObj.play()

player = Player(animObj, images[0].get_rect(), world, hit_sound)
player.x = 30000
player.y = 100


slime_frames = [pygame.image.load('./assets/images/slime1.png').convert(), pygame.image.load('./assets/images/slime2.png').convert()]
slime_ani = pyganim.PygAnimation(list(zip(slime_frames, [150] * len(slime_frames))))
slime_ani.set_alpha(160)
slime_ani.play()

slime_g_frames = [pygame.image.load('./assets/images/slime_green1.png').convert(), pygame.image.load('./assets/images/slime_green2.png').convert()]
slime_g_ani = pyganim.PygAnimation(list(zip(slime_g_frames, [150] * len(slime_g_frames))))
slime_g_ani.set_alpha(160)
slime_g_ani.play()

slime_p_frames = [pygame.image.load('./assets/images/slime_purple1.png').convert(), pygame.image.load('./assets/images/slime_purple2.png').convert()]
slime_p_ani = pyganim.PygAnimation(list(zip(slime_p_frames, [150] * len(slime_p_frames))))
slime_p_ani.set_alpha(160)
slime_p_ani.play()

pickaxe = pygame.image.load('./assets/images/pickaxe.png').convert()

mob_points = 32
mobs = []

snap_x = 0
snap_y = 0

# Main world camera 
cam = Camera(0, 0, width, height, world, player)

clock = pygame.time.Clock()

midnight = 0
time_of_day = 0.5

day_mask = (0,0,0)

lights_enabled = True

pygame.mouse.set_visible(0)

def update_day_mask():
    global midnight
    global time_of_day
    # Day night cycle
    inc = 0.0001
    if midnight:
        inc = -inc
    
    time_of_day += inc
    
    if time_of_day >= 1:
        midnight = 1
        time_of_day = 1

    if time_of_day <= 0:
        midnight = 0
        time_of_day = 0

    return tuple(numpy.multiply((128, 128, 128), (time_of_day, time_of_day, time_of_day)).astype(int))

hud_font = pygame.font.Font(os.path.join("assets", "fonts", 'main.ttf'), 15)
hud_font_item = pygame.font.Font(os.path.join("assets", "fonts", 'main.ttf'), 20)

def draw_stats(offset=(0,0)):
    left = width - 300
    blit_list = []
    # Health bar
    for x in range(player.max_health):
        y = 0
        empty = False
        if x > (player.health -1):
            empty = True
        if x >= 10:
            y = 1
            x = x - 10
        x_pos = (left + (x * 26))
        y_pos = (10 + (y * 26))
        pos = (x_pos, y_pos)
        if empty:
            blit_list.append((heart_none, pos))
        else:
            blit_list.append((heart, pos))

    for y in range(player.max_mana):
        x_pos = width - 30
        y_pos = (10 + (y * 26))
        pos = (x_pos, y_pos)
        if y > (player.mana -1):
            blit_list.append((mana_none, pos))
        else:
            blit_list.append((mana, pos))


    # Game data
    text = hud_font.render("World X {}".format(cam.x), 1, white)
    blit_list.append((text, (left ,0 + offset[1])))
    text = hud_font.render("World Y {}".format(cam.y), 1, white)
    blit_list.append((text, (left, 24 + offset[1])))
    text = hud_font.render("FPS {}".format(int(clock.get_fps())), 1, white)
    blit_list.append((text, (left, 48 + offset[1])))

    mouse_pos = pygame.mouse.get_pos()

    world_x = ((mouse_pos[0] + cam.x) // 16)
    world_y = ((mouse_pos[1] + cam.y) // 16)

    m_x = cam.x + mouse_pos[0]
    m_y = cam.y + mouse_pos[1]

    text = hud_font.render("M x:{} y:{}".format(int(mouse_pos[0]), int(mouse_pos[1])), 1, white)
    blit_list.append((text, (left, 72+ offset[1])))

    text = hud_font.render("W x:{} y:{}".format(world_x, world_y), 1, white)
    blit_list.append((text, (left, 96+ offset[1])))

    angle = player.get_angle( vec2(m_x, m_y) )
    text = hud_font.render("Angle :{}".format(angle), 1, white)
    blit_list.append((text, (left, 120+ offset[1])))

    dist = player.get_distance( vec2(m_x, m_y) )
    text = hud_font.render("Dist :{}".format(dist), 1, white)
    blit_list.append((text, (left, 144+ offset[1])))

    text = hud_font.render("Mob Size :{}".format(len(mobs)), 1, white)
    blit_list.append((text, (left, 168+ offset[1])))

    text = hud_font.render("Item Size :{}".format(len(world.items)), 1, white)
    blit_list.append((text, (left, 192+ offset[1])))

    # Items
    slot_size = 48
    for i in range(0, 10):
        x = 10
        if i > 0:
            x += (i * (slot_size + 5))
        item = None
        item_name = None
        item_count = None
        if i < len(player.items):
            item = player.items[i].icon()
            if player.items[i].maxStack > 1:
                item_count = player.items[i].stack
        border = (45, 20, 128)
        if i == player.equip:
            border = (255,255,255)
            if item:
                item_name = player.items[i].name
        round_rect(screen, pygame.Rect(x, 30, slot_size, slot_size), border, rad=6, border=2, inside=(45, 20, 128, 160))
        if item:
            blit_list.append((item, (x + (24 - (item.get_width() /2)), 30 + (24 - (item.get_height() / 2)))))
        if item_name:
            text = hud_font_item.render(item_name, 3, white)
            blit_list.append((text, (10, 5)))
        if item_count:
            text = hud_font.render("{}".format(item_count), 1, white)
            rect = text.get_rect()
            pos = (x + 4, (28 + slot_size) - rect.height)
            blit_list.append((text, pos))

        # Slot numbers
        slot = i + 1
        if slot == 10:
            slot = 0
        text = hud_font.render("{}".format(slot), 1, white)
        rect = text.get_rect()
        pos = (x + (slot_size - (rect.width + 7)), 32)
        blit_list.append((text, pos))

    blit_list.append((cursor, (mouse_pos[0] -8, mouse_pos[1] - 8)))

    screen.blits(blit_list, doreturn=False)

spawn_time = 4000
last_tick = pygame.time.get_ticks()

rot = 0

def rotate(surface, angle, pivot, offset):
    """Rotate the surface around the pivot point.

    Args:
        surface (pygame.Surface): The surface that is to be rotated.
        angle (float): Rotate by this angle.
        pivot (tuple, list, pygame.math.Vector2): The pivot point.
        offset (pygame.math.Vector2): This vector is added to the pivot.
    """
    rotated_image = pygame.transform.rotozoom(surface, -angle, 1)  # Rotate the image.
    rotated_offset = offset.rotate(angle)  # Rotate the offset vector.
    # Add the offset vector to the center/pivot point to shift the rect.
    rect = rotated_image.get_rect(center=pivot+rotated_offset)
    return rotated_image, rect  # Return the rotated image and shifted rect.

# Main game loop
while 1:

    mouse_pos = pygame.mouse.get_pos()

    globe_x = ((mouse_pos[0] + cam.x) - world.offset_x)
    globe_y = ((mouse_pos[1] + cam.y) - world.offset_y)

    snap_x = ((mouse_pos[0] // 16) * 16) - world.offset_x
    snap_y = ((mouse_pos[1] // 16) * 16) - world.offset_y

    world_x = (((mouse_pos[0] + cam.x)) // 16)
    world_y = (((mouse_pos[1] + cam.y)) // 16)

    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            sys.exit()
        if event.type == pygame.KEYUP:
            player.animation.pause()

    # This is where we check for key press's
    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_ESCAPE]:
        sys.exit()

    # Process Mouse events
    if pygame.mouse.get_pressed()[0]:
        if state == 0:
            state = 1
        if state == 1:       
            pass


    if state == 0:
        x1 += 2
        x += 2
        screen.blit(background,(x,y))
        screen.blit(background,(x1,y1))
        if x > w:
            x = -w
        if x1 > w:
            x1 = -w

        font = pygame.font.Font(os.path.join("assets", "fonts", 'main.ttf'), 48)
        text = font.render("Click to start", 1, white)
        r = text.get_rect()
        r.center = ((width / 2) - (r.width / 2), (height / 2) - (r.height / 2) ) 
        screen.blit(text, r.center)

    if state == 1:
        # Logic Update cycle
        now = pygame.time.get_ticks()
        if now > last_tick:
            if len(mobs) < mob_points:
                slime_type = random.choice([slime_ani, slime_g_ani, slime_p_ani])
                slime = Slime(slime_type, pygame.Rect(0,0,64,52), world)
                slime.x = random.choice([(cam.x - 200), (cam.x + cam.width + 200)])
                slime.y = 100
                mobs.append(slime)
                last_tick = now + (spawn_time + (spawn_time * random.random()))

        world.update(player)

        player.mouse_event(pygame.mouse.get_pressed()[0], world_x, world_y)
        player.key_event(pressed)
        player.update()

        cam.update()

        # Mob tracking
        if len(mobs) > 0:
            for mob in mobs:
                mob.update(player, world.projectiles)
            mobs[:] = [mob for mob in mobs if not mob.remove]

        if player.health <= 0:
            player.x = 30000
            player.y = 100
            player.health = 5

        # DRAWING CYCLE

        # Draw the main background, replace with a paralax class...
        screen.blit(background,(0,0))

        # Tile drawing, IE the world
        world.draw(screen, cam.x, cam.y, blocks)

        # Draw player and other sprites
        player.draw(screen, cam)

        for mob in mobs:
            mob.draw(screen, cam)

        # Lighting mask, day night and other light sources
        if lights_enabled:
            day_mask = update_day_mask()
            if day_mask[0] != 0:
                lighting(time_of_day, filter, screen, screen.get_width(), screen.get_height(), cam.x, cam.y, mouse_pos[0], mouse_pos[1], world)

        # Draw some of the world stats and menus
        draw_stats(offset=(0, 100))

        if rot < 360:
            rot = rot + 15
        else:
            rot = 0
        pickaxe_ = rotate(pickaxe, rot, [32,32], pygame.math.Vector2(-32, -32))
        #screen.blit(pickaxe_[0], (player.draw_pos[0] -48, player.draw_pos[1] -52))

        pygame.draw.line(screen, (100, 200, 255), (player.center.x - cam.x, player.center.y - cam.y), mouse_pos, 1)
    
    # Flip the screen and show to the user.
    pygame.display.flip()
    # Shouldn't need to worry about performance
    clock.tick(9999999)

